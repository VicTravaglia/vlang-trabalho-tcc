## Olá, Bem Vindo Ao Meu Projeto de TCC o Compilador Vlang!

### Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Java](https://openjdk.java.net/projects/jdk/11/) 11 ou em diante, [Maven](https://maven.apache.org/) 3.6.0 ou em diante.
### Rodando o Compilador Vlang

```bash
# Clone este repositório
$ git clone <https://VicTravaglia@bitbucket.org/VicTravaglia/vlang-trabalho-tcc.git>

# Acesse a pasta do projeto no terminal/cmd
$ cd vlang-trabalho-tcc

# Instale as dependências
$ mvn clean package

# Acesse a pasta do target após o build do projeto
$ cd target/

$ execute o comando java -jar vlang.jar -rt {PathDoArquivoVlang} {PathSaidaArquivos}
# O compilador será executado e gerará arquivos de saída no caminho especficado.
```
### Variáveis
#### A variável {PathDoArquivoVlang} é o caminho completo do arquivo vlang a ser lido.
#### A variável {PathSaidaArquivos} é o caminho para que a aplicação envie seus artefatos gerados após sua execução.

### Códigos de exemplo

#### Você pode encontrar códigos de exemplo da linguagem VLang nos seguintes links:
(https://bitbucket.org/VicTravaglia/vlang-trabalho-tcc/src/master/src/test/resources/)
(https://bitbucket.org/VicTravaglia/vlang-trabalho-tcc/src/master/src/main/resources/)