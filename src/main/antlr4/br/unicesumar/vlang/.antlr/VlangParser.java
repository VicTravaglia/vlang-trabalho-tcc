// Generated from /home/victor/Projects/vlang-compiler-tcc/src/main/antlr4/br/unicesumar/vlang/VlangParser.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class VlangParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		VARIABLE_TYPE=1, INTEGER_NUMBER=2, FLOAT_NUMBER=3, STRING=4, DECLARE=5, 
		ALGORITHM=6, COLON=7, LEFT_PAREN=8, RIGHT_PAREN=9, ASSIGN=10, TO=11, READ=12, 
		WRITE=13, IF=14, THEN=15, ELSE=16, WHILE=17, BEGIN=18, END=19, END_ALGORITHM=20, 
		ARITHMETIC_OPERATOR=21, RELATIONAL_OPERATOR=22, LOGICAL_OPERATOR=23, VARIABLE=24, 
		COMMENT=25, WS=26;
	public static final int
		RULE_program = 0, RULE_declaration = 1, RULE_arithmeticExpression = 2, 
		RULE_arithmeticTerm = 3, RULE_arithmeticFactor = 4, RULE_relationalExpression = 5, 
		RULE_relationalTerm = 6, RULE_command = 7, RULE_assignmentCommand = 8, 
		RULE_inputCommand = 9, RULE_outputCommand = 10, RULE_conditionCommand = 11, 
		RULE_repeatCommand = 12, RULE_subAlgorithm = 13;
	private static String[] makeRuleNames() {
		return new String[] {
			"program", "declaration", "arithmeticExpression", "arithmeticTerm", "arithmeticFactor", 
			"relationalExpression", "relationalTerm", "command", "assignmentCommand", 
			"inputCommand", "outputCommand", "conditionCommand", "repeatCommand", 
			"subAlgorithm"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, "'DECLARE'", "'ALGORITHM'", "':'", "'('", 
			"')'", "'ASSIGN'", "'TO'", "'READ'", "'WRITE'", "'IF'", "'THEN'", "'ELSE'", 
			"'WHILE'", "'BEGIN'", "'END'", "'END_ALGORITHM'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "VARIABLE_TYPE", "INTEGER_NUMBER", "FLOAT_NUMBER", "STRING", "DECLARE", 
			"ALGORITHM", "COLON", "LEFT_PAREN", "RIGHT_PAREN", "ASSIGN", "TO", "READ", 
			"WRITE", "IF", "THEN", "ELSE", "WHILE", "BEGIN", "END", "END_ALGORITHM", 
			"ARITHMETIC_OPERATOR", "RELATIONAL_OPERATOR", "LOGICAL_OPERATOR", "VARIABLE", 
			"COMMENT", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "VlangParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public VlangParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgramContext extends ParserRuleContext {
		public TerminalNode DECLARE() { return getToken(VlangParser.DECLARE, 0); }
		public TerminalNode ALGORITHM() { return getToken(VlangParser.ALGORITHM, 0); }
		public TerminalNode EOF() { return getToken(VlangParser.EOF, 0); }
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			match(DECLARE);
			setState(32);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==VARIABLE) {
				{
				{
				setState(29);
				declaration();
				}
				}
				setState(34);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(35);
			match(ALGORITHM);
			setState(39);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ASSIGN) | (1L << READ) | (1L << WRITE) | (1L << IF) | (1L << WHILE) | (1L << BEGIN))) != 0)) {
				{
				{
				setState(36);
				command();
				}
				}
				setState(41);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(42);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public TerminalNode VARIABLE() { return getToken(VlangParser.VARIABLE, 0); }
		public TerminalNode COLON() { return getToken(VlangParser.COLON, 0); }
		public TerminalNode VARIABLE_TYPE() { return getToken(VlangParser.VARIABLE_TYPE, 0); }
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(44);
			match(VARIABLE);
			setState(45);
			match(COLON);
			setState(46);
			match(VARIABLE_TYPE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArithmeticExpressionContext extends ParserRuleContext {
		public List<ArithmeticTermContext> arithmeticTerm() {
			return getRuleContexts(ArithmeticTermContext.class);
		}
		public ArithmeticTermContext arithmeticTerm(int i) {
			return getRuleContext(ArithmeticTermContext.class,i);
		}
		public List<TerminalNode> ARITHMETIC_OPERATOR() { return getTokens(VlangParser.ARITHMETIC_OPERATOR); }
		public TerminalNode ARITHMETIC_OPERATOR(int i) {
			return getToken(VlangParser.ARITHMETIC_OPERATOR, i);
		}
		public ArithmeticExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmeticExpression; }
	}

	public final ArithmeticExpressionContext arithmeticExpression() throws RecognitionException {
		ArithmeticExpressionContext _localctx = new ArithmeticExpressionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_arithmeticExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48);
			arithmeticTerm();
			setState(53);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ARITHMETIC_OPERATOR) {
				{
				{
				setState(49);
				match(ARITHMETIC_OPERATOR);
				setState(50);
				arithmeticTerm();
				}
				}
				setState(55);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArithmeticTermContext extends ParserRuleContext {
		public List<ArithmeticFactorContext> arithmeticFactor() {
			return getRuleContexts(ArithmeticFactorContext.class);
		}
		public ArithmeticFactorContext arithmeticFactor(int i) {
			return getRuleContext(ArithmeticFactorContext.class,i);
		}
		public List<TerminalNode> ARITHMETIC_OPERATOR() { return getTokens(VlangParser.ARITHMETIC_OPERATOR); }
		public TerminalNode ARITHMETIC_OPERATOR(int i) {
			return getToken(VlangParser.ARITHMETIC_OPERATOR, i);
		}
		public ArithmeticTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmeticTerm; }
	}

	public final ArithmeticTermContext arithmeticTerm() throws RecognitionException {
		ArithmeticTermContext _localctx = new ArithmeticTermContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_arithmeticTerm);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			arithmeticFactor();
			setState(61);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(57);
					match(ARITHMETIC_OPERATOR);
					setState(58);
					arithmeticFactor();
					}
					} 
				}
				setState(63);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArithmeticFactorContext extends ParserRuleContext {
		public TerminalNode INTEGER_NUMBER() { return getToken(VlangParser.INTEGER_NUMBER, 0); }
		public TerminalNode FLOAT_NUMBER() { return getToken(VlangParser.FLOAT_NUMBER, 0); }
		public TerminalNode VARIABLE() { return getToken(VlangParser.VARIABLE, 0); }
		public TerminalNode LEFT_PAREN() { return getToken(VlangParser.LEFT_PAREN, 0); }
		public ArithmeticExpressionContext arithmeticExpression() {
			return getRuleContext(ArithmeticExpressionContext.class,0);
		}
		public TerminalNode RIGHT_PAREN() { return getToken(VlangParser.RIGHT_PAREN, 0); }
		public ArithmeticFactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmeticFactor; }
	}

	public final ArithmeticFactorContext arithmeticFactor() throws RecognitionException {
		ArithmeticFactorContext _localctx = new ArithmeticFactorContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_arithmeticFactor);
		try {
			setState(71);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTEGER_NUMBER:
				enterOuterAlt(_localctx, 1);
				{
				setState(64);
				match(INTEGER_NUMBER);
				}
				break;
			case FLOAT_NUMBER:
				enterOuterAlt(_localctx, 2);
				{
				setState(65);
				match(FLOAT_NUMBER);
				}
				break;
			case VARIABLE:
				enterOuterAlt(_localctx, 3);
				{
				setState(66);
				match(VARIABLE);
				}
				break;
			case LEFT_PAREN:
				enterOuterAlt(_localctx, 4);
				{
				setState(67);
				match(LEFT_PAREN);
				setState(68);
				arithmeticExpression();
				setState(69);
				match(RIGHT_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalExpressionContext extends ParserRuleContext {
		public List<RelationalTermContext> relationalTerm() {
			return getRuleContexts(RelationalTermContext.class);
		}
		public RelationalTermContext relationalTerm(int i) {
			return getRuleContext(RelationalTermContext.class,i);
		}
		public List<TerminalNode> LOGICAL_OPERATOR() { return getTokens(VlangParser.LOGICAL_OPERATOR); }
		public TerminalNode LOGICAL_OPERATOR(int i) {
			return getToken(VlangParser.LOGICAL_OPERATOR, i);
		}
		public RelationalExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalExpression; }
	}

	public final RelationalExpressionContext relationalExpression() throws RecognitionException {
		RelationalExpressionContext _localctx = new RelationalExpressionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_relationalExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			relationalTerm();
			setState(78);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LOGICAL_OPERATOR) {
				{
				{
				setState(74);
				match(LOGICAL_OPERATOR);
				setState(75);
				relationalTerm();
				}
				}
				setState(80);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalTermContext extends ParserRuleContext {
		public List<ArithmeticExpressionContext> arithmeticExpression() {
			return getRuleContexts(ArithmeticExpressionContext.class);
		}
		public ArithmeticExpressionContext arithmeticExpression(int i) {
			return getRuleContext(ArithmeticExpressionContext.class,i);
		}
		public TerminalNode RELATIONAL_OPERATOR() { return getToken(VlangParser.RELATIONAL_OPERATOR, 0); }
		public TerminalNode LEFT_PAREN() { return getToken(VlangParser.LEFT_PAREN, 0); }
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public TerminalNode RIGHT_PAREN() { return getToken(VlangParser.RIGHT_PAREN, 0); }
		public RelationalTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalTerm; }
	}

	public final RelationalTermContext relationalTerm() throws RecognitionException {
		RelationalTermContext _localctx = new RelationalTermContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_relationalTerm);
		try {
			setState(89);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(81);
				arithmeticExpression();
				setState(82);
				match(RELATIONAL_OPERATOR);
				setState(83);
				arithmeticExpression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(85);
				match(LEFT_PAREN);
				setState(86);
				relationalExpression();
				setState(87);
				match(RIGHT_PAREN);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandContext extends ParserRuleContext {
		public AssignmentCommandContext assignmentCommand() {
			return getRuleContext(AssignmentCommandContext.class,0);
		}
		public InputCommandContext inputCommand() {
			return getRuleContext(InputCommandContext.class,0);
		}
		public OutputCommandContext outputCommand() {
			return getRuleContext(OutputCommandContext.class,0);
		}
		public ConditionCommandContext conditionCommand() {
			return getRuleContext(ConditionCommandContext.class,0);
		}
		public RepeatCommandContext repeatCommand() {
			return getRuleContext(RepeatCommandContext.class,0);
		}
		public SubAlgorithmContext subAlgorithm() {
			return getRuleContext(SubAlgorithmContext.class,0);
		}
		public CommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_command; }
	}

	public final CommandContext command() throws RecognitionException {
		CommandContext _localctx = new CommandContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_command);
		try {
			setState(97);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ASSIGN:
				enterOuterAlt(_localctx, 1);
				{
				setState(91);
				assignmentCommand();
				}
				break;
			case READ:
				enterOuterAlt(_localctx, 2);
				{
				setState(92);
				inputCommand();
				}
				break;
			case WRITE:
				enterOuterAlt(_localctx, 3);
				{
				setState(93);
				outputCommand();
				}
				break;
			case IF:
				enterOuterAlt(_localctx, 4);
				{
				setState(94);
				conditionCommand();
				}
				break;
			case WHILE:
				enterOuterAlt(_localctx, 5);
				{
				setState(95);
				repeatCommand();
				}
				break;
			case BEGIN:
				enterOuterAlt(_localctx, 6);
				{
				setState(96);
				subAlgorithm();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentCommandContext extends ParserRuleContext {
		public TerminalNode ASSIGN() { return getToken(VlangParser.ASSIGN, 0); }
		public ArithmeticExpressionContext arithmeticExpression() {
			return getRuleContext(ArithmeticExpressionContext.class,0);
		}
		public TerminalNode TO() { return getToken(VlangParser.TO, 0); }
		public TerminalNode VARIABLE() { return getToken(VlangParser.VARIABLE, 0); }
		public AssignmentCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentCommand; }
	}

	public final AssignmentCommandContext assignmentCommand() throws RecognitionException {
		AssignmentCommandContext _localctx = new AssignmentCommandContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_assignmentCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(99);
			match(ASSIGN);
			setState(100);
			arithmeticExpression();
			setState(101);
			match(TO);
			setState(102);
			match(VARIABLE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InputCommandContext extends ParserRuleContext {
		public TerminalNode READ() { return getToken(VlangParser.READ, 0); }
		public TerminalNode VARIABLE() { return getToken(VlangParser.VARIABLE, 0); }
		public InputCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inputCommand; }
	}

	public final InputCommandContext inputCommand() throws RecognitionException {
		InputCommandContext _localctx = new InputCommandContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_inputCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			match(READ);
			setState(105);
			match(VARIABLE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OutputCommandContext extends ParserRuleContext {
		public TerminalNode WRITE() { return getToken(VlangParser.WRITE, 0); }
		public ArithmeticExpressionContext arithmeticExpression() {
			return getRuleContext(ArithmeticExpressionContext.class,0);
		}
		public TerminalNode STRING() { return getToken(VlangParser.STRING, 0); }
		public OutputCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_outputCommand; }
	}

	public final OutputCommandContext outputCommand() throws RecognitionException {
		OutputCommandContext _localctx = new OutputCommandContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_outputCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			match(WRITE);
			setState(110);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTEGER_NUMBER:
			case FLOAT_NUMBER:
			case LEFT_PAREN:
			case VARIABLE:
				{
				setState(108);
				arithmeticExpression();
				}
				break;
			case STRING:
				{
				setState(109);
				match(STRING);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionCommandContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(VlangParser.IF, 0); }
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public TerminalNode THEN() { return getToken(VlangParser.THEN, 0); }
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(VlangParser.ELSE, 0); }
		public ConditionCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionCommand; }
	}

	public final ConditionCommandContext conditionCommand() throws RecognitionException {
		ConditionCommandContext _localctx = new ConditionCommandContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_conditionCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(112);
			match(IF);
			setState(113);
			relationalExpression();
			setState(114);
			match(THEN);
			setState(115);
			command();
			setState(118);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(116);
				match(ELSE);
				setState(117);
				command();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RepeatCommandContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(VlangParser.WHILE, 0); }
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public RepeatCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_repeatCommand; }
	}

	public final RepeatCommandContext repeatCommand() throws RecognitionException {
		RepeatCommandContext _localctx = new RepeatCommandContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_repeatCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(120);
			match(WHILE);
			setState(121);
			relationalExpression();
			setState(122);
			command();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubAlgorithmContext extends ParserRuleContext {
		public TerminalNode BEGIN() { return getToken(VlangParser.BEGIN, 0); }
		public TerminalNode END() { return getToken(VlangParser.END, 0); }
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public SubAlgorithmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subAlgorithm; }
	}

	public final SubAlgorithmContext subAlgorithm() throws RecognitionException {
		SubAlgorithmContext _localctx = new SubAlgorithmContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_subAlgorithm);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(124);
			match(BEGIN);
			setState(128);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ASSIGN) | (1L << READ) | (1L << WRITE) | (1L << IF) | (1L << WHILE) | (1L << BEGIN))) != 0)) {
				{
				{
				setState(125);
				command();
				}
				}
				setState(130);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(131);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\34\u0088\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\7\2!\n\2\f\2\16\2$\13"+
		"\2\3\2\3\2\7\2(\n\2\f\2\16\2+\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4"+
		"\7\4\66\n\4\f\4\16\49\13\4\3\5\3\5\3\5\7\5>\n\5\f\5\16\5A\13\5\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\5\6J\n\6\3\7\3\7\3\7\7\7O\n\7\f\7\16\7R\13\7\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\\\n\b\3\t\3\t\3\t\3\t\3\t\3\t\5\td\n"+
		"\t\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\5\fq\n\f\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\5\ry\n\r\3\16\3\16\3\16\3\16\3\17\3\17\7\17\u0081\n\17\f"+
		"\17\16\17\u0084\13\17\3\17\3\17\3\17\2\2\20\2\4\6\b\n\f\16\20\22\24\26"+
		"\30\32\34\2\2\2\u008a\2\36\3\2\2\2\4.\3\2\2\2\6\62\3\2\2\2\b:\3\2\2\2"+
		"\nI\3\2\2\2\fK\3\2\2\2\16[\3\2\2\2\20c\3\2\2\2\22e\3\2\2\2\24j\3\2\2\2"+
		"\26m\3\2\2\2\30r\3\2\2\2\32z\3\2\2\2\34~\3\2\2\2\36\"\7\7\2\2\37!\5\4"+
		"\3\2 \37\3\2\2\2!$\3\2\2\2\" \3\2\2\2\"#\3\2\2\2#%\3\2\2\2$\"\3\2\2\2"+
		"%)\7\b\2\2&(\5\20\t\2\'&\3\2\2\2(+\3\2\2\2)\'\3\2\2\2)*\3\2\2\2*,\3\2"+
		"\2\2+)\3\2\2\2,-\7\2\2\3-\3\3\2\2\2./\7\32\2\2/\60\7\t\2\2\60\61\7\3\2"+
		"\2\61\5\3\2\2\2\62\67\5\b\5\2\63\64\7\27\2\2\64\66\5\b\5\2\65\63\3\2\2"+
		"\2\669\3\2\2\2\67\65\3\2\2\2\678\3\2\2\28\7\3\2\2\29\67\3\2\2\2:?\5\n"+
		"\6\2;<\7\27\2\2<>\5\n\6\2=;\3\2\2\2>A\3\2\2\2?=\3\2\2\2?@\3\2\2\2@\t\3"+
		"\2\2\2A?\3\2\2\2BJ\7\4\2\2CJ\7\5\2\2DJ\7\32\2\2EF\7\n\2\2FG\5\6\4\2GH"+
		"\7\13\2\2HJ\3\2\2\2IB\3\2\2\2IC\3\2\2\2ID\3\2\2\2IE\3\2\2\2J\13\3\2\2"+
		"\2KP\5\16\b\2LM\7\31\2\2MO\5\16\b\2NL\3\2\2\2OR\3\2\2\2PN\3\2\2\2PQ\3"+
		"\2\2\2Q\r\3\2\2\2RP\3\2\2\2ST\5\6\4\2TU\7\30\2\2UV\5\6\4\2V\\\3\2\2\2"+
		"WX\7\n\2\2XY\5\f\7\2YZ\7\13\2\2Z\\\3\2\2\2[S\3\2\2\2[W\3\2\2\2\\\17\3"+
		"\2\2\2]d\5\22\n\2^d\5\24\13\2_d\5\26\f\2`d\5\30\r\2ad\5\32\16\2bd\5\34"+
		"\17\2c]\3\2\2\2c^\3\2\2\2c_\3\2\2\2c`\3\2\2\2ca\3\2\2\2cb\3\2\2\2d\21"+
		"\3\2\2\2ef\7\f\2\2fg\5\6\4\2gh\7\r\2\2hi\7\32\2\2i\23\3\2\2\2jk\7\16\2"+
		"\2kl\7\32\2\2l\25\3\2\2\2mp\7\17\2\2nq\5\6\4\2oq\7\6\2\2pn\3\2\2\2po\3"+
		"\2\2\2q\27\3\2\2\2rs\7\20\2\2st\5\f\7\2tu\7\21\2\2ux\5\20\t\2vw\7\22\2"+
		"\2wy\5\20\t\2xv\3\2\2\2xy\3\2\2\2y\31\3\2\2\2z{\7\23\2\2{|\5\f\7\2|}\5"+
		"\20\t\2}\33\3\2\2\2~\u0082\7\24\2\2\177\u0081\5\20\t\2\u0080\177\3\2\2"+
		"\2\u0081\u0084\3\2\2\2\u0082\u0080\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0085"+
		"\3\2\2\2\u0084\u0082\3\2\2\2\u0085\u0086\7\25\2\2\u0086\35\3\2\2\2\r\""+
		")\67?IP[cpx\u0082";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}