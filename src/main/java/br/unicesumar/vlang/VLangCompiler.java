package br.unicesumar.vlang;

import br.unicesumar.vlang.compiler.CompilerExecutor;
import br.unicesumar.vlang.compiler.CompilerFactory;
import br.unicesumar.vlang.error.ErrorHandler;
import picocli.CommandLine;

import static picocli.CommandLine.*;

@Command(name = "Vlang", description = "The Vlang compiler")
public class VLangCompiler implements Runnable {

    @Option(names = {"-r", "--run"}, description = "Runs the compiled vlang code", defaultValue = "false")
    private boolean runOption;

    @Option(names = {"-t", "--tree"}, description = "Generates parser tree and symbol table", defaultValue = "false")
    private boolean generateOption;

    @Parameters(description = "The Vlang file to be compiled")
    private String programFile;

    @Parameters(description = "The output path to the compiled file")
    private String outputPath;

    private CompilerFactory compilerFactory;

    public static void main(String[] args) {
        Runtime.getRuntime().exit(new CommandLine(new VLangCompiler()).execute(args));
    }

    @Override
    public void run() {
        ErrorHandler errorHandler = new ErrorHandler();

        CompilerExecutor compilerExecutor = new CompilerFactory().newInstance(errorHandler);

        compilerExecutor.execute(programFile, outputPath, runOption, generateOption);

        if (!errorHandler.getErrors().isEmpty()) {
            errorHandler.getErrors().forEach(System.err::println);
        }
    }
}
