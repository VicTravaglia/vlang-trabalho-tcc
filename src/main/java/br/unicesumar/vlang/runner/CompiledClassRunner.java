package br.unicesumar.vlang.runner;

import org.mdkt.compiler.InMemoryJavaCompiler;

import static br.unicesumar.vlang.error.ErrorMessages.EXECUTION_VLANG_FILE_ERROR;
import static br.unicesumar.vlang.utils.ConstantUtils.FULLY_QUALIFIED_VLANG_COMPILED_CLASS_NAME;
import static br.unicesumar.vlang.utils.ConstantUtils.VLANG_MAIN_METHOD_NAME;

final public class CompiledClassRunner {

    public static void run(String outputCode) {
        try {
            InMemoryJavaCompiler.newInstance()
                .compile(FULLY_QUALIFIED_VLANG_COMPILED_CLASS_NAME, outputCode)
                .getDeclaredMethod(VLANG_MAIN_METHOD_NAME, String[].class)
                .invoke(null, (Object) null);
        } catch (Exception ignored) {
            System.err.println(EXECUTION_VLANG_FILE_ERROR);
        }
    }
}


