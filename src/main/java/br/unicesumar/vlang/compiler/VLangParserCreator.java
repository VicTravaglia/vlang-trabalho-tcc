package br.unicesumar.vlang.compiler;

import br.unicesumar.vlang.VlangLexer;
import br.unicesumar.vlang.VlangParser;
import br.unicesumar.vlang.error.listener.ErrorListener;
import org.antlr.v4.runtime.CommonTokenStream;

final public class VLangParserCreator {

    private final ErrorListener errorListener;

    public VLangParserCreator(ErrorListener errorListener) {
        this.errorListener = errorListener;
    }

    public VlangParser create(VlangLexer lexer) {
        VlangParser parser = new VlangParser(new CommonTokenStream(lexer));

//        parser.removeErrorListeners();
//
//        parser.addErrorListener(errorListener);

        return parser;
    }
}
