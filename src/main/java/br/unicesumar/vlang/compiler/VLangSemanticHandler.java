package br.unicesumar.vlang.compiler;

import br.unicesumar.vlang.VlangParser;
import br.unicesumar.vlang.compiler.model.SymbolTable;
import br.unicesumar.vlang.compiler.model.VLangType;
import br.unicesumar.vlang.error.ErrorHandler;

import java.util.concurrent.atomic.AtomicReference;

import static br.unicesumar.vlang.error.ErrorMessages.*;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

final public class VLangSemanticHandler {

    private final ErrorHandler errors;

    public VLangSemanticHandler(ErrorHandler errors) {
        this.errors = errors;
    }

    public VLangType verifyType(SymbolTable table, VlangParser.ArithmeticExpressionContext ctx) {
        AtomicReference<VLangType> type = new AtomicReference<>();

        ctx.arithmeticTerm().forEach(arithmeticTermContext -> {
            VLangType actualType = verifyType(table, arithmeticTermContext);

            if (isNull(type.get())) {
                type.set(actualType);
            } else if (type.get() != actualType && actualType != VLangType.INVALID) {
                errors.addSemanticError(String.format(EXPRESSION_TYPE_COMPATIBILITY_ERROR, ctx.start.getType(), ctx.getText()));
                type.set(VLangType.INVALID);
            }
        });

        return type.get();
    }

    private VLangType verifyType(SymbolTable table, VlangParser.ArithmeticTermContext ctx) {
        AtomicReference<VLangType> type = new AtomicReference<>();

        ctx.arithmeticFactor().forEach(arithmeticFactorContext -> {
            VLangType actualType = verifyType(table, arithmeticFactorContext);

            if (isNull(type.get())) {
                type.set(actualType);
            } else if (type.get() != actualType && actualType != VLangType.INVALID) {
                errors.addSemanticError(String.format(TERM_TYPE_COMPATIBILITY_ERROR, ctx.start.getType(), ctx.getText()));
                type.set(VLangType.INVALID);
            }
        });

        return type.get();
    }

    private VLangType verifyType(SymbolTable table, VlangParser.ArithmeticFactorContext ctx) {
        if (nonNull(ctx.INTEGER_NUMBER())) {
            return VLangType.INTEGER;
        }

        if (nonNull(ctx.FLOAT_NUMBER())) {
            return VLangType.FLOAT;
        }

        if (nonNull(ctx.VARIABLE())) {
            String variableName = ctx.VARIABLE().getText();

            if (table.exists(variableName)) {
                return verifyType(table, variableName);
            }

            errors.addSemanticError(String.format(VARIABLE_NOT_DECLARE_ERROR, variableName));
        }

        return verifyType(table, ctx.arithmeticExpression());
    }

    public VLangType verifyType(SymbolTable table, String variableName) {
        return table.verifyType(variableName);
    }

}
