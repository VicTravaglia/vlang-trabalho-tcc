package br.unicesumar.vlang.compiler;

import br.unicesumar.vlang.VlangLexer;
import br.unicesumar.vlang.VlangParser;
import br.unicesumar.vlang.compiler.model.Symbol;
import br.unicesumar.vlang.dot.DotOptions;
import br.unicesumar.vlang.dot.DotTreeRepresentation;
import br.unicesumar.vlang.dot.SimpleTree;
import br.unicesumar.vlang.runner.CompiledClassRunner;
import guru.nidi.graphviz.engine.Engine;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import org.antlr.v4.runtime.CharStreams;

import javax.print.PrintException;
import java.io.*;
import java.util.Map;

import static br.unicesumar.vlang.VlangParser.ProgramContext;
import static br.unicesumar.vlang.error.ErrorMessages.CREATION_VLANG_FILE_ERROR;
import static br.unicesumar.vlang.error.ErrorMessages.OPEN_FILE_ERROR;
import static br.unicesumar.vlang.utils.ConstantUtils.VLANG_COMPILED_CLASS_NAME;
import static java.lang.System.lineSeparator;


final public class CompilerExecutor {

    private final VLangLexerCreator vlangLexerCreator;

    private final VLangParserCreator vlangParserCreator;

    private final VLangParserVisitor vlangParserVisitor;

    private final VLangCodeGenerator vlangCodeGenerator;

    public CompilerExecutor(VLangLexerCreator vlangLexerCreator,
                            VLangParserCreator vlangParserCreator,
                            VLangParserVisitor vlangParserVisitor,
                            VLangCodeGenerator VLangCodeGeneratorVisitor) {
        this.vlangLexerCreator = vlangLexerCreator;
        this.vlangParserCreator = vlangParserCreator;
        this.vlangParserVisitor = vlangParserVisitor;
        this.vlangCodeGenerator = VLangCodeGeneratorVisitor;
    }

    public void execute(String programFile, String outputPath, boolean runOption, boolean generateOption) {
        try {
            VlangLexer lexer = vlangLexerCreator.create(CharStreams.fromFileName(programFile.trim()));

            VlangParser parser = vlangParserCreator.create(lexer);
            parser.setBuildParseTree(true);

            ProgramContext program = parser.program();

            vlangParserVisitor.visitProgram(program);

            String generatedCode = vlangCodeGenerator.generate(program, outputPath);

            if (runOption) {
                CompiledClassRunner.run(generatedCode);
            }

            if (generateOption) {
                createSymbolTable(vlangCodeGenerator.getTable(), outputPath);
                createTreeImage(outputPath, parser, program);
            }
        } catch (Exception exception) {
            treatException(programFile, exception);
        }
    }

    private void createSymbolTable(Map<String, Symbol> table, String outputPath) throws FileNotFoundException {
        try (PrintWriter printWriter = new PrintWriter(outputPath + "symbolTable.txt")) {
            table.forEach((key, value) -> printWriter.println("Simbolo: "
                    + key
                    + " | "
                    + " Tipo: " + value.getType()
                    + " Nome: " + value.getName()));
        }
    }

    private void createTreeImage(String outputPath, VlangParser parser, ProgramContext program) throws IOException {
        SimpleTree tree = new SimpleTree.Builder()
                .withParser(parser)
                .withParseTree(program)
                .withDisplaySymbolicName(false)
                .build();

        DotOptions options = new DotOptions.Builder()
                .withParameters("  labelloc=\"t\";\n  label=\"Expression Tree\";\n\n")
                .withLexerRuleShape("circle")
                .build();

        Graphviz.fromString(new DotTreeRepresentation().display(tree, options))
                .engine(Engine.DOT)
                .render(Format.PNG)
                .toFile(new File(outputPath + "treeDOT.png"));
    }

    private void treatException(String programFile, Exception exception) {
        if (exception instanceof FileNotFoundException) {
            System.err.println(CREATION_VLANG_FILE_ERROR);
        } else if (exception instanceof IOException) {
            System.err.printf(OPEN_FILE_ERROR, programFile.trim(), lineSeparator());
        } else if (exception instanceof IllegalCallerException) {
            System.err.println(exception.getMessage());
        }
    }
}
