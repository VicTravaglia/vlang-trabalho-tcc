package br.unicesumar.vlang.compiler.model;

public enum VLangType {
    INTEGER,
    FLOAT,
    INVALID;
}
