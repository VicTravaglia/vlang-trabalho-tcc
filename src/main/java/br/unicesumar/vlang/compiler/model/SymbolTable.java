package br.unicesumar.vlang.compiler.model;

import java.util.HashMap;
import java.util.Map;

public class SymbolTable {

    private final Map<String, Symbol> table = new HashMap<>();

    public void add(String name, VLangType type) {
        table.put(name, new Symbol(name, type));
    }

    public boolean exists(String name) {
        return table.containsKey(name);
    }

    public VLangType verifyType(String name) {
        return table.get(name).getType();
    }

    public Map<String, Symbol> getTable() {
        return table;
    }
}
