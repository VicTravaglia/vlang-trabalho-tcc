package br.unicesumar.vlang.compiler.model;

public class Symbol {

    private final String name;
    private final VLangType type;

    public Symbol(String name, VLangType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public VLangType getType() {
        return type;
    }
}
