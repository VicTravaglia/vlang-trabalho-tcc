package br.unicesumar.vlang.compiler;

import br.unicesumar.vlang.error.ErrorHandler;
import br.unicesumar.vlang.error.listener.ErrorListener;

final public class CompilerFactory {

    public CompilerExecutor newInstance(ErrorHandler errorHandler) {

        ErrorListener errorListener = new ErrorListener(errorHandler);

        VLangSemanticHandler vLangSemanticHandler = new VLangSemanticHandler(errorHandler);

        return new CompilerExecutor(
                new VLangLexerCreator(errorListener),
                new VLangParserCreator(errorListener),
                new VLangParserVisitor(vLangSemanticHandler, errorHandler),
                new VLangCodeGenerator(vLangSemanticHandler, errorHandler));
    }
}
