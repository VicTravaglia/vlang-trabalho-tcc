package br.unicesumar.vlang.compiler;

import br.unicesumar.vlang.VlangParser;
import br.unicesumar.vlang.VlangParserBaseVisitor;
import br.unicesumar.vlang.compiler.model.SymbolTable;
import br.unicesumar.vlang.compiler.model.VLangType;
import br.unicesumar.vlang.error.ErrorHandler;

import static br.unicesumar.vlang.error.ErrorMessages.*;

final public class VLangParserVisitor extends VlangParserBaseVisitor<Void> {

    private final VLangSemanticHandler vLangSemanticHandler;

    private final ErrorHandler errorHandler;

    private SymbolTable table;

    public VLangParserVisitor(VLangSemanticHandler vLangSemanticHandler, ErrorHandler errorHandler) {
        this.vLangSemanticHandler = vLangSemanticHandler;
        this.errorHandler = errorHandler;
    }

    @Override
    public Void visitProgram(VlangParser.ProgramContext ctx) {
        table = new SymbolTable();

        return super.visitProgram(ctx);
    }

    @Override
    public Void visitDeclaration(VlangParser.DeclarationContext ctx) {
        String variableName = ctx.VARIABLE().getText();
        String variableType = ctx.VARIABLE_TYPE().getText();

        VLangType type;

        switch (variableType) {
            case "INTEGER":
                type = VLangType.INTEGER;
                break;
            case "FLOAT":
                type = VLangType.FLOAT;
                break;
            default:
                type = VLangType.INVALID;
                break;
        }

        if (table.exists(variableName)) {
            errorHandler.addSemanticError(ctx.VARIABLE().getSymbol(), String.format(VARIABLE_ALREADY_EXISTS_ERROR, variableName));
        } else {
            table.add(variableName, type);
        }

        return super.visitDeclaration(ctx);
    }

    @Override
    public Void visitAssignmentCommand(VlangParser.AssignmentCommandContext ctx) {
        VLangType type = vLangSemanticHandler.verifyType(table, ctx.arithmeticExpression());

        if (!type.equals(VLangType.INVALID)) {
            String variableName = ctx.VARIABLE().getText();

            if (!table.exists(variableName)) {
                errorHandler.addSemanticError(ctx.VARIABLE().getSymbol(), String.format(VARIABLE_NOT_DECLARE_ERROR, variableName));
            }

            if (!vLangSemanticHandler.verifyType(table, variableName).equals(type)) {
                errorHandler.addSemanticError(ctx.VARIABLE().getSymbol(), String.format(VARIABLE_TYPE_COMPATIBILITY_ERROR, variableName));
            }
        }

        return super.visitAssignmentCommand(ctx);
    }

    @Override
    public Void visitInputCommand(VlangParser.InputCommandContext ctx) {
        String variableName = ctx.VARIABLE().getText();

        if (!table.exists(variableName)) {
            errorHandler.addSemanticError(ctx.VARIABLE().getSymbol(), VARIABLE_NOT_DECLARE_ERROR);
        }

        return super.visitInputCommand(ctx);
    }

    @Override
    public Void visitArithmeticExpression(VlangParser.ArithmeticExpressionContext ctx) {
        vLangSemanticHandler.verifyType(table, ctx);

        return super.visitArithmeticExpression(ctx);
    }

}
