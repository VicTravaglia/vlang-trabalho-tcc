package br.unicesumar.vlang.compiler;

import br.unicesumar.vlang.VlangLexer;
import br.unicesumar.vlang.error.listener.ErrorListener;
import org.antlr.v4.runtime.CharStream;

final public class VLangLexerCreator {

    private final ErrorListener errorListener;

    public VLangLexerCreator(ErrorListener errorListener) {
        this.errorListener = errorListener;
    }

    public VlangLexer create(CharStream input) {
        VlangLexer lexer = new VlangLexer(input);

//        lexer.removeErrorListeners();
//
//        lexer.addErrorListener(errorListener);

        return lexer;
    }
}
