package br.unicesumar.vlang.utils;

import java.util.Scanner;

final public class ConstantUtils {

    public static String VLANG_COMPILED_CLASS_NAME = "VlangCompiledClass";
    public static String FULLY_QUALIFIED_VLANG_COMPILED_CLASS_NAME = "br.unicesumar.vlang.VlangCompiledClass";
    public static String VLANG_MAIN_METHOD_NAME = "main";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Integer fatorial = 0;
        Integer valor = 0;
        Integer contador = 0;

        contador = 1;
        fatorial = 1;
        System.out.println("Calculo de fatorial");
        System.out.println("Informe o numero a ser calculado: ");
        valor = scanner.nextInt();

        while (contador <= valor)
            fatorial = fatorial + (fatorial * (valor - 1));
        valor = valor - 1;
        System.out.println("O resultado do fatorial é:");
        System.out.println(fatorial + valor);

    }

}
