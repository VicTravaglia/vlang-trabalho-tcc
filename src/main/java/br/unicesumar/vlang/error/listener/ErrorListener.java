package br.unicesumar.vlang.error.listener;

import br.unicesumar.vlang.error.ErrorHandler;
import org.antlr.v4.runtime.*;

public class ErrorListener extends BaseErrorListener {

    private final ErrorHandler errorHandler;

    public ErrorListener(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer,
                            Object offendingSymbol,
                            int line,
                            int charPositionInLine,
                            String msg,
                            RecognitionException e) {
        if (e instanceof LexerNoViableAltException) {
            errorHandler.addLexerError(line, charPositionInLine, msg);
        }

        if (e instanceof InputMismatchException) {
            errorHandler.addSyntaxError(line, charPositionInLine, msg);
        }

        if (e instanceof NoViableAltException) {
            errorHandler.addSyntaxError(line, charPositionInLine, msg);
        }

        if (e instanceof FailedPredicateException) {
            errorHandler.addSemanticError(line, charPositionInLine, msg);
        }
    }
}
