package br.unicesumar.vlang.error;

public final class ErrorMessages {

    public static final String VARIABLE_NOT_DECLARE_ERROR = "variable %s was not declared before use.";
    public static final String VARIABLE_TYPE_COMPATIBILITY_ERROR = "variable type %s is not compatible with expression type.";
    public static final String TERM_TYPE_COMPATIBILITY_ERROR = "%s term %s contains incompatible types.";
    public static final String EXPRESSION_TYPE_COMPATIBILITY_ERROR = "%s expression %s contains incompatible types.";
    public static final String VARIABLE_ALREADY_EXISTS_ERROR = "variable %s has already been defined in this scope.";
    public static final String SYNTAX_ERROR = "Syntactic Error %d:%d - %s";
    public static final String LEXER_ERROR = "Lexical Error %d:%d - %s";
    public static final String SEMANTIC_ERROR_WITH_TOKEN = "Semantic Error %d:%d - %s";
    public static final String SEMANTIC_ERROR_WITHOUT_TOKEN = "Semantic Error - %s";
    public static final String RUNNING_VLANG_CODE_ERROR = "An error occurred while running the vlang code.";
    public static final String OPEN_FILE_ERROR = "Could not open file: %s %s";
    public static final String CREATION_VLANG_FILE_ERROR = "Could not create Vlang compiled file.";
    public static final String EXECUTION_VLANG_FILE_ERROR = "Could not execute Vlang compiled file.";

}
