package br.unicesumar.vlang.error;

import org.antlr.v4.runtime.Token;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static br.unicesumar.vlang.error.ErrorMessages.*;
import static java.lang.String.format;

final public class ErrorHandler {

    private final List<String> errors;

    public ErrorHandler() {
        errors = new ArrayList<>();
    }

    public void addLexerError(int line, int charPositionInLine, String error) {
        errors.add(format(LEXER_ERROR, line, charPositionInLine, error));
    }

    public void addSyntaxError(int line, int charPositionInLine, String error) {
        errors.add(format(SYNTAX_ERROR, line, charPositionInLine, error));
    }

    public void addSemanticError(int line, int charPositionInLine, String error) {
        errors.add(format(SEMANTIC_ERROR_WITH_TOKEN, line, charPositionInLine, error));
    }

    public void addSemanticError(Token token, String error) {
        errors.add(format(SEMANTIC_ERROR_WITH_TOKEN, token.getLine(), token.getLine(), error));
    }

    public void addSemanticError(String error) {
        errors.add(format(SEMANTIC_ERROR_WITHOUT_TOKEN, error));
    }

    public List<String> getErrors() {
        return Collections.unmodifiableList(errors);
    }

}
