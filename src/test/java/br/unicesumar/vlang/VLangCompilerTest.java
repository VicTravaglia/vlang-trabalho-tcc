package br.unicesumar.vlang;

import org.junit.jupiter.api.Test;

class VLangCompilerTest {

    @Test
    public void shouldExecuteVlangFactorial() {
        String[] param = {"-r", "src/test/resources/VlangFactorial", "src/test/resources/"};
        VLangCompiler.main(param);
    }

    @Test
    public void shouldExecuteVlangBasicMath() {
        String[] param = {"-r", "src/test/resources/VlangBasicMathForTest", "src/test/resources/"};
        VLangCompiler.main(param);
    }
}